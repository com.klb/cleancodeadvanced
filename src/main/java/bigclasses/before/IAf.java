package bigclasses.before;

public interface IAf
{
	void f1();
	void f2();
	void f3();
	void fn();
}
