package bigclasses.before;

public interface IAg
{
	void g1( );

	void g2( );

	void gk( );
}
