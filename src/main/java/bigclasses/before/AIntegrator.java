package bigclasses.before;

public class AIntegrator implements IAf, IAg
{
	private IAf iaf;
	private IAg iag;



	@Override public void f1( )
	{
		iaf.f1();
	}

	@Override public void f2( )
	{

	}

	@Override public void f3( )
	{

	}

	@Override public void fn( )
	{

	}

	@Override public void g1( )
	{
		iag.g1();
	}

	@Override public void g2( )
	{

	}

	@Override public void gk( )
	{

	}
}
