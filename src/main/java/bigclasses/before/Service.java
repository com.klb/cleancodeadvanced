package bigclasses.before;

public class Service
{
	private final IA a;

	public Service( IA a )
	{
		this.a = a;
	}

	public void start() {
		a.f1();
		a.g2();
	}

	public void fun1() {
		a.g2();
	}

	public void fun2() {
		a.gk();
	}
}
