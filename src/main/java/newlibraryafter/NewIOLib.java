package newlibraryafter;

public class NewIOLib
{
	private String  file;
		
	public NewIOLib(String file) {
		super();
		this.file = file;
	}

	//metoda niezgodna z uzywanym w projekcie interfejsem
	public String scanFile() {
		return "Reading from " + file + " using new lib";
	}
}
