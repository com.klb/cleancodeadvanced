package newlibraryafter;

public class NewIOLibAdapter implements ISimpleIO
{
	private NewIOLib lib = null;
	
	public NewIOLibAdapter(String file)
	{
		lib = new NewIOLib(file);
	}
	
	//metoda zgodna ze starym interfejsem
	@Override
	public String readFromFile() {
		return lib.scanFile();
		
	}

}
