package newlibraryafter;

public class OldFileReader implements ISimpleIO
{

	private String path;
	
	public void setPath(String path) {
		this.path = path;
	}

	@Override
	public String readFromFile() {
		return "Reading from " + path + " using old lib";
		
	}

}
