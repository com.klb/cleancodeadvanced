package newlibraryafter;

public class Main
{
	public static void main(String[] args) {
		//stare uzycie
		//		OldFileReader oldReader = new OldFileReader();
		//		oldReader.setPath("c:\test.txt");
		//		logicForFile(oldReader);
		//		checkFile(oldReader);

		//nowe uzycie  ------

		//NewIOLib newReader = new NewIOLib("c:\test.txt");
		//logicForFile(newReader);
		//checkFile(newReader); //nie mozemy tego zrobic bo jest niezgodnosc interfejsow
//
		//wiec stosujemy Adapter
		NewIOLibAdapter newReader = new NewIOLibAdapter("c:\test.txt");
		ClientService.logicForFile(newReader);
		ClientService.checkFile(newReader);

	}
}
