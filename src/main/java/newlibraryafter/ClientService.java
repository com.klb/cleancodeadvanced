package newlibraryafter;
//przyklad zastosowania wzorca projektowego Adapter
//mozna zastosowac gdy trzeba uzgodnic np. nowa biblioteke wprowadzana do projektu
//a co za tym idzie inne nazwy metod ze starym interfejsem (starej biblioteki)

//metoda nowej wprowadzanej biblioteki jest niezgodna z interfejsem stosowanym
//w calym projekcie wiec zamiast wprowadzac wiele zmian w projekcie, stosujemy wzorzec Adapter
public class ClientService
{
	
	//wiele metod projektu uzywajacych stary interfejs obslugi czytania z plikow
	public static void logicForFile( ISimpleIO freader)
	{
		System.out.println("additional logic...");
		freader.readFromFile();
	}
	
	public static void checkFile( ISimpleIO freader)
	{
		System.out.println("additional logic...");
		freader.readFromFile();
	}


}
