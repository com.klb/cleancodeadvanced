package videochannelafterrefactor;


import java.util.ArrayList;
import java.util.List;


public class VideoChannel
{
    private String name;
    private List<String> movieTitles = new ArrayList<>();
    private List<Subscriber>  subscribers = new ArrayList<>();

    public VideoChannel( String name )
    {
        this.name = name;
    }

    public String getName( )
    {
        return name;
    }

    public void addMovieTitle(String movieTitle) {
        movieTitles.add(movieTitle);
        informSubscribers();
    }

    public String getLatestTitlle() {
        return movieTitles.get(  movieTitles.size() - 1);
    }

    public void addSubscriber(Subscriber subscriber) {
        subscribers.add(subscriber);
    }

    private void informSubscribers() {
        for(Subscriber s : subscribers) {
                s.inform( );
            }
    }
}

