package videochannelafterrefactor;

//Observer
public abstract class Subscriber
{
	protected VideoChannel videoChannel;

	public Subscriber( VideoChannel videoChannel )
	{
		this.videoChannel = videoChannel;
	}

	public abstract void inform();
}
