package videochannelafterrefactor;

public class EmailSubscriber extends Subscriber
{
	private String email;

	public EmailSubscriber(String email, VideoChannel videoChannel) {
		super(videoChannel);
		this.email = email;
		videoChannel.addSubscriber( this );
	}
	@Override
	public void inform( )
	{
		String channelName = videoChannel.getName();
		String latestMovie = videoChannel.getLatestTitlle();
		System.out.println(String.format("Sending email %s: new film %s has been added to %s channel",
			email, latestMovie, channelName));
	}
}
