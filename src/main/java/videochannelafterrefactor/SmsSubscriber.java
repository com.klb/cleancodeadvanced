package videochannelafterrefactor;

public class SmsSubscriber extends Subscriber
{
	private String phoneNumber;

	public SmsSubscriber(String phoneNumber, VideoChannel videoChannel) {
		super(videoChannel);
		this.phoneNumber = phoneNumber;
		videoChannel.addSubscriber( this );
	}
	@Override
	public void inform( )
	{
		String channelName = videoChannel.getName();
		String latestMovie = videoChannel.getLatestTitlle();
		System.out.println(String.format("Sending sms %s: new film %s has been added to %s channel",
			phoneNumber, latestMovie, channelName));
	}
}
