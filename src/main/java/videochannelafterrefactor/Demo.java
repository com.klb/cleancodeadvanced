package videochannelafterrefactor;

public class Demo
{
	public static void main( String[] args )
	{
		VideoChannel videoChannel = new VideoChannel("Mathematics");
		Subscriber s1 = new EmailSubscriber("Kasia", videoChannel);
		Subscriber s2 = new SmsSubscriber("Basia", videoChannel);

		videoChannel.addMovieTitle("Introduction to topology");
	}
}
