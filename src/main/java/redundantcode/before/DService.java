package redundantcode.before;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class DService
{
	private DService() {

	}

	private static  Set<String> filterToSet(List<String> list) {
		return list.stream().filter( s -> s.startsWith( "A" ) ||  s.startsWith( "B" ))
			.map( String::toUpperCase )
			.collect( Collectors.toSet());
	}

	public static Set<String> fun1( List<String> list, List<String> words) {
		Set<String> res = filterToSet(list);

		words.forEach( res::remove );
		return res;
	}

	public static Set<String> fun2(List<String> list, List<String> words) {
		Set<String> res = filterToSet(list);

		words.forEach( res::add );
		return res;
	}


}
