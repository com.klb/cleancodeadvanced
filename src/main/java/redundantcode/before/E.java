package redundantcode.before;

public abstract class E
{
	protected void fn1() {
		System.out.println("some logic of fn1");
	}

	protected void h() {
		System.out.println("some logic of h");
	}
}
