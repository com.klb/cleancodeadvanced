package testdecomp;

public class ServiceF
{
	public int f(int x) {
		if(x % 2 == 0) {
			return 777;
		}

		return x <= 0 ? 2*x: x +1;
	}

}
