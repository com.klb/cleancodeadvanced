package torefactortask.before;

public class Service1
{
	public void f1() {
		//Here is the logic of Service1 f1
		SpecialLogger logger = new SpecialLogger();
		logger.info( "Service1 f1 has been completed" );
	}

	public void f2() {
		SpecialLogger logger = new SpecialLogger();
		logger.info( "Service1 f2 has been started" );
		//Here is the logic of Service1 f2
	}

	public void fk() {
		//Here is the logic of Service1 fk
	}
}
