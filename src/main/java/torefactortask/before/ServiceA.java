package torefactortask.before;

public class ServiceA
{
	private Service1 service1 = new Service1();
	private Service2 service2 = new Service2();

	public void execute() {
		SpecialLogger logger = new SpecialLogger();
		logger.info( "ServiceA execute has been started" );
		service1.f1();
		//some logic
		service2.f2();
	}
}
