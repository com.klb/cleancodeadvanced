package torefactortask.before;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class SpecialLogger
{
	public void info(String message) {
		var dtf = DateTimeFormatter.ofPattern("uuuu/MM/dd HH:mm:ss");
		var now = LocalDateTime.now();
		var sdatetime = dtf.format(now);
		System.out.println(String.format( "%s INFO: %s", sdatetime, message  ));
	}

	public void error(String message) {
		var dtf = DateTimeFormatter.ofPattern("uuuu/MM/dd HH:mm:ss");
		var now = LocalDateTime.now();
		var sdatetime = dtf.format(now);
		System.out.println(String.format( "%s ERROR: %s", sdatetime, message  ));
	}

	public void warning(String message) {
		var dtf = DateTimeFormatter.ofPattern("uuuu/MM/dd HH:mm:ss");
		var now = LocalDateTime.now();
		var sdatetime = dtf.format(now);
		System.out.println(String.format( "%s WARNING: %s", sdatetime, message  ));
	}
}
