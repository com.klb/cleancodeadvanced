package torefactortask.before;

public class Service2
{
	public void f1() {
		SpecialLogger logger = new SpecialLogger();
		logger.info( "Service2 f1 has been started" );
		//Here is the logic of Service2 f1
	}

	public void f2() {
		//Here is the logic of Service2 f2
	}

	public void fk() {
		//Here is the logic of Service2 fk
		SpecialLogger logger = new SpecialLogger();
		logger.info( "Service2 fk has been completed" );
	}
}
