package dependencies.companychat.before.after;



public class Main {
    public static void main(String[] args) {
        TeamManager teamManager = new TeamManager();
        Employee emp1 = new Employee("123", "kasia", teamManager);
        Employee emp2 = new Employee("444", "basia", teamManager);
        Employee emp3 = new Employee("555", "zosia", teamManager);
        teamManager.addEmployee(emp1);
        teamManager.addEmployee(emp2);
        teamManager.addEmployee(emp3);

        emp1.sendMessageToEveryone("Hello team!");
    }
}
