package dependencies.companychat.before.after;

import java.util.HashMap;
import java.util.Map;

/*
        e2 -  m - e1

        n + n = 2n
 */
/* klasa Mediatora */
public class TeamManager {

    private Map<String, Employee> empployees  = new HashMap<>();

    public void addEmployee(Employee emp) {
        empployees.put(emp.getId(), emp);
    }

    public void sendMessageToEveryone(String message, Employee fromEmployee) {
        for(Map.Entry<String, Employee> pair : empployees.entrySet()) {
            if(!fromEmployee.getId().equals(pair.getKey())) {
                pair.getValue().receiveMessage(message, fromEmployee.getName());
            }
        }
    }


}
