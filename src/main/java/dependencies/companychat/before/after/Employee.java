package dependencies.companychat.before.after;

import java.util.Objects;

public class Employee {
    private String id;
    private String name;

    private TeamManager teamManager;

    public Employee(String id, String name, TeamManager teamManager) {
        this.id = id;
        this.name = name;
        this.teamManager = teamManager;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void sendMessageToEveryone(String message) {
        teamManager.sendMessageToEveryone(message, this);
    }

    public void receiveMessage(String message, String fromEmployeeName) {
        System.out.println(String.format("%s has received message %s from %s", name, message, fromEmployeeName));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return Objects.equals(id, employee.id) && Objects.equals(name, employee.name) && Objects.equals(teamManager, employee.teamManager);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
}
