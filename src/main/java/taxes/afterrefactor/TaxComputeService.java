package taxes.afterrefactor;

public class TaxComputeService
{
	private ITax tax;

	public void setTaxType(TaxType taxType) {
		switch(taxType) {
			case PL -> {
				tax = new TaxPl();
			}
			case EN -> {
				tax = new TaxEn();
			}
		}
	}

	public double compute(double v) {
		return tax.compute( v );
	}
}
