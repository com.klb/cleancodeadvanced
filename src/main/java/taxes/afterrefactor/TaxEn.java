package taxes.afterrefactor;

public class TaxEn implements ITax
{
	@Override public double compute( double value )
	{
		//a lot of lines of code, for example for getting actual tax rate from external service
		double taxtRate = 0.3;
		return taxtRate * value;
	}
}
