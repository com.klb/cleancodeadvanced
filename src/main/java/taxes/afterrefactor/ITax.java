package taxes.afterrefactor;

public interface ITax {
	double compute(double value);
}