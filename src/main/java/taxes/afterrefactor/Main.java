package taxes.afterrefactor;

public class Main
{
	private static TaxComputeService taxComputeService = new TaxComputeService();

	public static void main( String[] args )
	{
		taxComputeService.setTaxType( TaxType.PL );
		System.out.println(taxComputeService.compute( 2000.0 ));

		taxComputeService.setTaxType( TaxType.EN );
		System.out.println(taxComputeService.compute( 2000.0 ));
	}
}
