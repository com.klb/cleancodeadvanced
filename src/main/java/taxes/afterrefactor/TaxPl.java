package taxes.afterrefactor;

public class TaxPl implements ITax
{
	@Override public double compute( double value )
	{
		//a lot of lines of code, for example for getting actual tax rate from external service
		double taxtRate = 0.25;
		return taxtRate * value;
	}
}
