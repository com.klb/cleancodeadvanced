package largesetofclasses.afterrefactor;
/*
	Abstraction - GUIElementRenderer
		AbstrType1 - GUIElementinLinuxStyle
		AbstrType2 - GUIElementinWindowsStyle


	Widget --> GUIElementRenderer

	Implementation ~ Widget
		ConcreteImpl1 - Button
		ConcreteImpl2 - CheckedBox

	f(n, k) = 2 + n + k
 */
public class Main
{
	public static void main( String[] args )
	{
		GUIElementRenderer linuxButton = new GUIElementRendererInLinuxStyle( new Button(101, 201) );
		linuxButton.render();

		GUIElementRenderer windowsChecBox = new GUIElementRendererInWindowsStyle( new Button(51, 51) );
		windowsChecBox.render();
	}
}
