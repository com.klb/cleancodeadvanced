package largesetofclasses.afterrefactor;

public abstract class Widget
{
	protected int x, y;

	public Widget( int x, int y )
	{
		this.x = x;
		this.y = y;
	}

	public abstract String getWidgetRepresentation();
}
