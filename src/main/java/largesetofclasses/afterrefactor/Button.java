package largesetofclasses.afterrefactor;

public class Button extends Widget
{
	public Button( int x, int y )
	{
		super( x, y );
	}

	@Override public String getWidgetRepresentation( )
	{
		return String.format("Button. x=%d y=%d", x, y);
	}
}
