package largesetofclasses.afterrefactor;

public class GUIElementRendererInLinuxStyle extends GUIElementRenderer
{
	public GUIElementRendererInLinuxStyle(Widget renderElement) {
		super(renderElement);
	}

	@Override public void render( )
	{
		System.out.println("Started rendering in Linux style");
		System.out.println( renderElement.getWidgetRepresentation() );
		System.out.println("Completed rendering in Linux style");
	}
}
