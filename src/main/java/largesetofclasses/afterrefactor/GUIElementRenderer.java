package largesetofclasses.afterrefactor;

import largesetofclasses.afterrefactor.Widget;

public abstract class GUIElementRenderer
{
	protected Widget renderElement;

	public GUIElementRenderer( Widget renderElement )
	{
		this.renderElement = renderElement;
	}

	public abstract void render();
}
