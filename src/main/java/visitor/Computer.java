package visitor;

public class Computer extends Part
{
	public Computer( int price, String name )
	{
		super( price, name );
	}

	@Override public void accept( IComputerVisitor v )
	{
		v.visit( this );
	}
}
