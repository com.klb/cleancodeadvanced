package visitor;

public class Main
{
	public static void main( String[] args )
	{
//		Computer computer = new Computer( 1000, "abc" );
//		InfoVisitor infoVisitor = new InfoVisitor();
//		PriceChangerVisitor priceChangerVisitor = new PriceChangerVisitor();
//		computer.accept( infoVisitor );
//		computer.accept( priceChangerVisitor );
//		System.out.println(computer.getPrice());

		new Service(new InfoVisitor()).execute();
	}
}
