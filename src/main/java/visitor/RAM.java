package visitor;

public class RAM extends Part
{
	public RAM( int price, String name )
	{
		super( price, name );
	}

	@Override public void accept( IComputerVisitor v )
	{
		v.visit( this );
	}
}
