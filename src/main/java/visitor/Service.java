package visitor;

public class Service
{
	private IComputerVisitor visitor;

	public Service( IComputerVisitor visitor )
	{
		this.visitor = visitor;
	}

//	public void setVisitor( IComputerVisitor visitor )
//	{
//		this.visitor = visitor;
//	}

	public void execute() {
		// ...
		Computer computer = new Computer( 1000, "abc" );
		computer.accept( visitor );
		System.out.println(computer.getPrice());
	}
}
