package visitor;

public interface IComputerVisitor
{
	void visit(Processor p);
	void visit(RAM r);
	void visit(Computer r);
}
