package visitor;

public class PriceChangerVisitor implements IComputerVisitor
{
	@Override public void visit( Processor p )
	{
		p.setPrice( p.getPrice() *2);
	}

	@Override public void visit( RAM r )
	{
		r.setPrice( r.getPrice() *3);
	}

	@Override public void visit( Computer r )
	{
		r.setPrice( r.getPrice() *2);
	}
}
