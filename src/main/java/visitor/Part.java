package visitor;

public abstract class Part
{
	private int price;
	private String name;

	public abstract  void accept( IComputerVisitor v );

	public Part( int price, String name )
	{
		this.price = price;
		this.name = name;
	}

	public int getPrice( )
	{
		return price;
	}

	public void setPrice( int price )
	{
		this.price = price;
	}

	public String getName( )
	{
		return name;
	}

	public void setName( String name )
	{
		this.name = name;
	}
}
