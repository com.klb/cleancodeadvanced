package visitor;

public class InfoVisitor implements IComputerVisitor
{
	@Override public void visit( Processor p )
	{
		System.out.println( String.format( "InfoVisitor odwiedza procesor: Cena %s Nazwa: %s", p.getPrice(), p.getName() ) );
	}

	@Override public void visit( RAM r )
	{
		System.out.println( String.format( "InfoVisitor odwiedza RAM: Cena %s Nazwa: %s", r.getPrice(), r.getName() ) );
	}

	@Override public void visit( Computer r )
	{
		System.out.println( String.format( "InfoVisitor odwiedza komputer: Cena %s Nazwa: %s", r.getPrice(), r.getName() ) );
	}
}
