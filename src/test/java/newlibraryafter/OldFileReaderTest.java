package newlibraryafter;

import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.*;

public class OldFileReaderTest
{

	//@Ignore
	@Test
	public void readFromFile( )
	{
		OldFileReader oldFileReader = new OldFileReader();
		oldFileReader.setPath( "test" );

		assertEquals("Reading from test using old lib", oldFileReader.readFromFile());
	}
}