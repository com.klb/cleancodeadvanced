package testdecomp;

import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class ServiceFTest
{
	private static ServiceF serviceF;

	@BeforeClass
	public static void setUp() {
		serviceF = new ServiceF();
	}

	@Test
	public void testFCase1( )
	{
		assertEquals(777, serviceF.f( 20 ));
	}

	@Test
	public void testFCase2( )
	{
		assertEquals(4, serviceF.f( 3 ));
	}

}